package tests;

import game.Vector2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class Vector2Test {

	/** Test comparison of vectors. */
	@Test
	void testComp1() {
		Vector2 v1 = new Vector2(2, 4);
		Vector2 v2 = new Vector2(-0.1f, 5);
		Vector2 v3 = new Vector2(4f, -5);
		Vector2 actual1 = new Vector2(2f, 4f);
		Vector2 actual2 = new Vector2(-0.1f, 5f);
		Vector2 notActual1 = new Vector2(2, -0.8f);
		
		assertTrue(v1.equals(actual1));
		assertTrue(v2.equals(actual2));
		
		assertFalse(v3.equals(notActual1));
	}
	
	/** Test comparison of vectors with tolerance. */
	@Test
	void testComp2() {
		Vector2 v1 = new Vector2(2, 4);
		Vector2 v2 = new Vector2(-0.1f, 5);
		Vector2 v3 = new Vector2(4f, -5);
		float delta1 = 1;
		float delta2 = 0.2f;
		float delta3 = 0.5f;
		float illegalDelta = -1;
		Vector2 expected1 = new Vector2(2.5f, 3.8f);
		Vector2 expected2 = new Vector2(-0.15f, 5.05f);
		Vector2 notExpected1 = new Vector2(4, 0);
		
		assertTrue(v1.equals(expected1, delta1));
		assertTrue(v2.equals(expected2, delta2));
		assertFalse(v3.equals(notExpected1, delta3));
		try {
			v1.equals(expected1, illegalDelta);
			fail("IllegalArgumentException expected");
		} catch (java.lang.IllegalArgumentException expected) {
			assertEquals("'delta' must be non-negative.", expected.getMessage());
		}
	}
	
	/** Test square magnitude of vectors. */
	@Test
	void testSqrMagnitude() {
		Vector2 v1 = new Vector2(3, 4);
		Vector2 v2 = new Vector2(5.2f, -4);
		Vector2 v3 = new Vector2(1, 1);
		float expected1 = 25;
		float expected2 = 43.04f;
		float expected3 = 2;
		
		assertEquals(v1.sqrMagnitude(), expected1, 0.01f);
		assertEquals(v2.sqrMagnitude(), expected2, 0.01f);
		assertEquals(v3.sqrMagnitude(), expected3, 0.01f);
	}
	
	/** Test magnitude of vectors. */
	@Test
	void testMagnitude() {
		Vector2 v1 = new Vector2(3, 4);
		Vector2 v2 = new Vector2(5.2f, -4);
		Vector2 v3 = new Vector2(1, 1);
		float expected1 = 5;
		float expected2 = 6.56f;
		float expected3 = 1.41f;
		
		assertEquals(v1.magnitude(), expected1, 0.01f);
		assertEquals(v2.magnitude(), expected2, 0.01f);
		assertEquals(v3.magnitude(), expected3, 0.01f);
	}
	
	/** Test normalization of vectors. */
	@Test
	void testNormalized() {
		Vector2 v1 = new Vector2(3, 4);
		Vector2 v2 = new Vector2(0, -4);
		Vector2 v3 = new Vector2(1, 1);
		Vector2 expected1 = new Vector2(0.6f, 0.8f);
		Vector2 expected2 = new Vector2(0, -1f);
		Vector2 expected3 = new Vector2(0.71f, 0.71f);
		Vector2 illegalVector = new Vector2(0, 0);
		
		assertTrue(v1.normalized().equals(expected1));
		assertTrue(v2.normalized().equals(expected2));
		assertTrue(v3.normalized().equals(expected3, 0.01f));
		try {
			illegalVector.normalized();
			fail("ArithmeticException expected");
		} catch (java.lang.ArithmeticException expected) {
			assertEquals("Magnitude of vector is zero and therfore has no direction.",
						 expected.getMessage());
		}
	}
	
	/** Test addition of vectors. */
	@Test
	void testAdd() {
		Vector2 v1 = new Vector2(2, 4f);
		Vector2 v2 = new Vector2(-5, 1.5f);
		Vector2 v3 = new Vector2(-0.1f, 5f);
		Vector2 v4 = new Vector2(-1f, -0.3f);
		Vector2 v5 = new Vector2(4f, -5);
		Vector2 v6 = new Vector2(2, -0.8f);
		Vector2 expected1 = new Vector2(-3f, 5.5f);
		Vector2 expected2 = new Vector2(-1.1f, 4.7f);
		Vector2 notExpected1 = new Vector2(6, -5.9f);
		
		assertTrue(v1.add(v2).equals(expected1));
		assertTrue(v3.add(v4).equals(expected2));
		assertFalse(v5.add(v6).equals(notExpected1));
	}
	
	/** Test subtraction of vectors. */
	@Test
	void testSub() {
		Vector2 v1 = new Vector2(2, 4f);
		Vector2 v2 = new Vector2(-5, 1.5f);
		Vector2 v3 = new Vector2(-0.1f, 5f);
		Vector2 v4 = new Vector2(-1f, -0.3f);
		Vector2 v5 = new Vector2(4f, -5);
		Vector2 v6 = new Vector2(2, -0.8f);
		Vector2 expected1 = new Vector2(7f, 2.5f);
		Vector2 expected2 = new Vector2(0.9f, 5.3f);
		Vector2 notExpected1 = new Vector2(2, -4.3f);
		
		assertTrue(v1.sub(v2).equals(expected1));
		assertTrue(v3.sub(v4).equals(expected2));
		assertFalse(v5.sub(v6).equals(notExpected1));
	}
	
	/** Test dot product of vectors. */
	@Test
	void testDot() {
		Vector2 v1 = new Vector2(2, 4f);
		Vector2 v2 = new Vector2(-5, 1.5f);
		Vector2 v3 = new Vector2(-0.1f, 5f);
		Vector2 v4 = new Vector2(-1f, -0.3f);
		Vector2 v5 = new Vector2(4f, -5);
		Vector2 v6 = new Vector2(2, -0.8f);
		float expected1 = -4f;
		float expected2 = -1.4f;
		float notExpected1 = 11;
		
		assertEquals(v1.dot(v2), expected1);
		assertEquals(v3.dot(v4), expected2);
		assertNotEquals(v5.dot(v6), notExpected1);
	}
	
	/** Test scaling vectors by numbers. */
	@Test
	void testMul() {
		Vector2 v1 = new Vector2(2, 4f);
		Vector2 v2 = new Vector2(-0.1f, 5f);
		Vector2 v3 = new Vector2(4f, -5);
		float a1 = 3.5f;
		float a2 = 0;
		float a3 = 1f;
		Vector2 expected1 = new Vector2(7f, 14);
		Vector2 expected2 = new Vector2(0, 0f);
		Vector2 notExpected1 = new Vector2(5, -5);
		
		assertTrue(v1.mul(a1).equals(expected1));
		assertTrue(v2.mul(a2).equals(expected2));
		assertFalse(v3.mul(a3).equals(notExpected1));
	}
	
	/** Test dividing vectors by numbers. */
	@Test
	void testDiv() {
		Vector2 v1 = new Vector2(2, 4f);
		Vector2 v2 = new Vector2(-0.1f, 5f);
		Vector2 v3 = new Vector2(4f, -5);
		float a1 = 4;
		float a2 = 0f;
		float a3 = 1f;
		Vector2 expected1 = new Vector2(0.5f, 1);
		Vector2 expected2 = new Vector2(Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY);
		Vector2 notExpected1 = new Vector2(5, -5);
		
		assertTrue(v1.div(a1).equals(expected1));
		assertTrue(v2.div(a2).equals(expected2));
		assertFalse(v3.mul(a3).equals(notExpected1));
	}
}
