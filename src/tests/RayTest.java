package tests;

import game.Ray;
import game.Vector2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RayTest {

	/** Test the getPoint method of Ray. */
	@Test
	void testGetPoint() {
		Ray ray = new Ray(new Vector2(-2, 0.1f), new Vector2(2, 3));
		float distance1 = 4.5f;
		float distance2 = -0.5f;
		float distance3 = 0;
		float distance4 = 1;
		float distance5 = 3;
		Vector2 expected1 = new Vector2(0.5f, 3.84f);
		Vector2 expected2 = new Vector2(-2.28f, -0.32f);
		Vector2 expected3 = new Vector2(-2, 0.1f); // Same as origin of ray.
		Vector2 expected4 = new Vector2(-1.45f, 0.93f); // Same as origin + direction of ray.
		Vector2 notExpected = new Vector2(3, 3);
		
		assertAll(
				() -> assertTrue(ray.getPoint(distance1).equals(expected1, 0.01f)),
				() -> assertTrue(ray.getPoint(distance2).equals(expected2, 0.01f)),
				() -> assertTrue(ray.getPoint(distance3).equals(expected3, 0.01f)),
				() -> assertTrue(ray.getPoint(distance4).equals(expected4, 0.01f)),
				() -> assertFalse(ray.getPoint(distance5).equals(notExpected, 0.01f)));
	}

}
