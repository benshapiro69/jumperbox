package game;

import processing.core.PApplet;

/** Et episk lille gamer-spil. */
public class JumperBox extends PApplet {
	
	public static boolean playing; 
	
	// Is the user pressing the "a" or left arrow key?
	public boolean pressingLeft = false;
	// Is the user pressing the "d" or right arrow key?
	public boolean pressingRight = false;
	// Is the user pressing a jump key?
	public boolean pressingJump = false;
	
	// Static singleton instance.
	static JumperBox jb = null;
	
	public void settings() {
		jb = this;
		fullScreen();
	}
	
	public void setup() {
		frameRate(60);
		Level.loadLevel(0);
		playing = true;
		noCursor();
	}
	
	public void draw() {
		clear();
		background(255);
		Level.loadedLevel.updateObjects();
	}
	
	public void keyPressed() {
		if (key == 'a' || key == 'A' || keyCode == LEFT) {
			pressingLeft = true;
			if (playing) {
				Level.loadedLevel.getPlayer().facingRight = false;
			}
		}
		if (key == 'd' || key == 'D' || keyCode == RIGHT) {
			pressingRight = true;
			if (playing) {
				Level.loadedLevel.getPlayer().facingRight = true;
			}
		}
		if (key == ' ' || key == 'w' || key == 'W' || keyCode == UP) {
			pressingJump = true;
		}
	}
	public void keyReleased() {
		if (key == 'a' || key == 'A' || keyCode == LEFT) {
			pressingLeft = false;
			if(pressingRight == true){
				if (playing) {
					Level.loadedLevel.getPlayer().facingRight = true;
				}
			}
		}
		if (key == 'd' || key == 'D' || keyCode == RIGHT) {
			pressingRight = false;
			if(pressingLeft == true){
				if (playing) {
					Level.loadedLevel.getPlayer().facingRight = false;
				}
			}
		}
		if (key == ' ' || key == 'w' || key == 'W' || keyCode == UP) {
			pressingJump = false;
		}
	}
}
