package game;

import processing.core.PFont;

public class Ketchup extends Sign {

	static String imagePath = "data/ketchup.png";
	static PFont font = jb.createFont("Verdana", 48);
	
	boolean babbet = false;
	
	public Ketchup(String name, Vector2 position, Vector2 scale) {
		super(name, position, scale, "Ketchup status: BABBET", Vector2.zero(), 48);
		signImage = jb.loadImage(imagePath);
		signImage.resize((int) Transform.toScreenScale(scale).x, -(int) Transform.toScreenScale(scale).y);
	}
	
	public Ketchup(Vector2 position, Vector2 scale) {
		this("ketchup", position, scale);
	}
	
	@Override
	public void show() {
		if (!babbet) {
			super.show();
		}
	}
	
	@Override
	public void showText() {
		super.showText();
		babbet = true;
	}
}