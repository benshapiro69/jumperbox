package game;

import java.util.Comparator;

import processing.core.PApplet;

/** 2D Vector class */
public class Vector2 {
	
	public float x, y;
	
	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2() {
		this.x = 0;
		this.y = 0;
	}
	
	/** Alternative to writing Vector2(0, -1). */
	public static Vector2 down() {
		return new Vector2(0, -1);
	}
	
	/** Alternative to writing Vector2(1, 0). */
	public static Vector2 right() {
		return new Vector2(1, 0);
	}
	
	/** Alternative to writing Vector2(0, 1). */
	public static Vector2 up() {
		return new Vector2(0, 1);
	}
	
	/** Alternative to writing Vector2(-1, 0). */
	public static Vector2 left() {
		return new Vector2(-1, 0);
	}
	
	/** Alternative to writing Vector2(1, 1). */
	public static Vector2 one() {
		return new Vector2(1, 1);
	}
	
	/** Alternative to writing Vector2(0, 0). */
	public static Vector2 zero() {
		return new Vector2(0, 0);
	}
	
	/** The distance between 'a' and 'b'. */
	public static float distance(Vector2 a, Vector2 b) {
		return a.sub(b).magnitude();
	}
	
	/** Linearly interpolate between a and b by t. */
	public static Vector2 lerp(Vector2 a, Vector2 b, float t) {
		return b.sub(a).mul(t).add(a);
	}
	
	/** Compare the x and y value of this and one other vector */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj==null || obj.getClass()!=this.getClass()) return false;
		
		boolean xEquals = this.x == ((Vector2) obj).x;
		boolean yEquals = this.y == ((Vector2) obj).y;
		return xEquals && yEquals;
	}
	
	/** Compare the x and y value of this and one other vector to within a non-negative delta. */
	public boolean equals(Vector2 v, float delta) {
		if (delta < 0) {
			throw new java.lang.IllegalArgumentException("'delta' must be non-negative.");
		}
		
		boolean xEqual = this.x - delta/2 <= v.x && this.x + delta/2 >= v.x;
		boolean yEqual = this.y - delta/2 <= v.y && this.y + delta/2 >= v.y;
		
		return xEqual && yEqual;
	}
	
	/** Return the square magnitude of this vector */
	public float sqrMagnitude() {
		float result = this.x * this.x + this.y * this.y;
		
		return result;
	}
	
	/** Return the magnitude of this vector */
	public float magnitude() {
		float result = PApplet.sqrt(this.sqrMagnitude());
		
		return result;
	}
	
	/** Return the normalized version of this vector */
	public Vector2 normalized() {
		float magnitude = this.magnitude();
		if (magnitude == 0) {
			throw new java.lang.ArithmeticException("Magnitude of vector is zero and therfore has no direction.");
		}
		
		Vector2 result = new Vector2();
		result.x = this.x / magnitude;
		result.y = this.y / magnitude;
		
		return result;
	}
	
	/** Return the addition of this and one other vector */
	public Vector2 add(Vector2 v) {
		Vector2 result = new Vector2();
		result.x = this.x + v.x;
		result.y = this.y + v.y;

		return result;
	}
	
	/** Return the subtraction of this and one other vector */
	public Vector2 sub(Vector2 v) {
		Vector2 result = new Vector2();
		result.x = this.x - v.x;
		result.y = this.y - v.y;

		return result;
	}
	
	/** Return this vector scaled by a number */
	public Vector2 mul(float a) {
		Vector2 result = new Vector2();
		result.x = this.x * a;
		result.y = this.y * a;

		return result;
	}
	
	/** Return this vector divided by a number */
	public Vector2 div(float a) {
		Vector2 result = new Vector2();
		result.x = this.x / a;
		result.y = this.y / a;

		return result;
	}
	
	/** Return the dot product of this and one other vector */
	public float dot(Vector2 v) {
		float result = this.x * v.x + this.y * v.y;

		return result;
	}
	
	/** Return the distance from this vector to 'v'. */
	public float distance(Vector2 v) {
		return distance(this, v);
	}
	
	/** Return a vector rotated around 'pivot' by 'theta' radians. */
	public Vector2 rotate (Vector2 pivot, float theta) {
		Vector2 centered = sub(pivot);
		float rx = PApplet.cos(theta) * centered.x + PApplet.sin(theta) * centered.y;
		float ry = -PApplet.sin(theta) * centered.x + PApplet.cos(theta) * centered.y;
		return pivot.add(new Vector2(rx, ry)); 
	}
	
	/** Rotate a vector by 'theta' radians */
	public Vector2 rotate(float theta) {
		return rotate(zero(), theta);
	}
}

/** Compare vectors based off of magnitude. */
class MagnitudeComparator implements Comparator<Vector2> {
	
	@Override
	public int compare(Vector2 o1, Vector2 o2) {
		float mag1 = o1.magnitude();
		float mag2 = o2.magnitude();

		int result = mag1 <  mag2 ? -1 : mag1 == mag2 ? 0 : 1;
		return result;
	}
}