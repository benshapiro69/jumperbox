package game;

/** Representation of mathematical rays. */
public class Ray {
	
	private Vector2 origin, direction;
	
	/** Construct ray starting at 'origin' pointing in 'direction'.
	 * 
	 * 'direction' may not have a magnitude of zero.
	 * */
	public Ray(Vector2 origin, Vector2 direction) {
		setOrigin(origin);
		setDirection(direction);
	}
	
	/** Get the origin of the ray. */
	public Vector2 getOrigin() {
		return origin;
	}
	
	/** Set the origin of the ray. */
	public void setOrigin(Vector2 origin) {
		this.origin = origin;
	}
	
	/** Get the direction of the ray.
	 * 
	 * This always has a magnitude of 1.
	 *  */
	public Vector2 getDirection() {
		return direction;
	}
	
	/** Set the direction of the ray.
	 * 
	 * 'direction' may not have a magnitude of zero.
	 *  */
	public void setDirection(Vector2 direction) {
		this.direction = direction.normalized();
	}
	
	/** Get a point at 'distance' along the ray. */
	public Vector2 getPoint(float distance) {
		return origin.add(direction.mul(distance));
	}
	
	public void show(float r, float g, float b) {
		Vector2 screenOrigin = Transform.toScreenPoint(origin);
		Vector2 screenDirection = new Vector2(direction.x, -direction.y);
		
		JumperBox jb = JumperBox.jb;
		Vector2 lineEnd = screenOrigin.add(screenDirection .mul(jb.width + jb.height));
		
		jb.stroke(r, g, b);
		jb.line(screenOrigin.x, screenOrigin.y, 
				lineEnd.x, lineEnd.y);
	}
	
	public void show(float value) {
		show(value, value, value);
	}
	
	public void show() {
		show(0);
	}
}
