package game;

/** Class for drawing objects in the scene. */
public class Gizmos {

	static JumperBox jb = JumperBox.jb;

	// RGB colors.
	static int r, g, b;

	/** Get the RGB colors as an array. */
	public static int[] getColor() {
		int[] rgb = { r, g, b };
		return rgb;
	}

	/**
	 * Set the RGB color.
	 * 
	 * 'r', 'g' and 'b' must be in the range [0 : 256[.
	 */
	public static void setColor(int r, int g, int b) {
		if (r < 0 || r >= 256 || g < 0 || g >= 256 || b < 0 || b >= 256) {
			throw new java.lang.IllegalArgumentException("'r', 'g' and " + "'b' must be in the range [0 : 256[");
		}
		Gizmos.r = r;
		Gizmos.g = g;
		Gizmos.b = b;
	}

	/* Set the RGB color all equal to value. */
	public static void setColor(int value) {
		if (value < 0 || value >= 256) {
			throw new java.lang.IllegalArgumentException("'value' must " + "be in the range [0 : 256[");
		}
		r = g = b = value;
	}

	/** Draw a dot at position with specified strokeWeight. */
	public static void drawPoint(Vector2 position, float strokeWeight) {
		Vector2 screenPos = Transform.toScreenPoint(position);

		jb.strokeWeight(strokeWeight);
		jb.stroke(r, g, b);
		jb.point(screenPos.x, screenPos.y);
	}

	/** Draw a vector from (0, 0) to 'v1' with specified stroke weight. */
	public static void drawVector(Vector2 v1, float strokeWeight) {
		Vector2 sv1 = Transform.toScreenPoint(v1);

		jb.strokeWeight(strokeWeight);
		jb.stroke(r, g, b);
		jb.line(0, 0, sv1.x, sv1.y);

		// Draw arrow head.
		jb.fill(r, b, g);
		Vector2 sv2 = sv1.mul(0.9f).add(new Vector2(-sv1.y, sv1.x).mul(0.02f));
		Vector2 sv3 = sv1.mul(0.9f).add(new Vector2(-sv1.y, sv1.x).mul(-0.02f));
		jb.triangle(sv1.x, sv1.y, sv2.x, sv2.y, sv3.x, sv3.y);
	}

	/** Draw a vector from (0, 0) to 'v1'. */
	public static void drawVector(Vector2 v1) {
		drawVector(v1, 1);
	}

	/**
	 * Draw a line with coordinates 'start' and 'start'+'dir' with specified stroke
	 * weight.
	 */
	public static void drawRay(Vector2 start, Vector2 dir, float strokeWeight) {
		Vector2 screenStart = Transform.toScreenPoint(start);
		Vector2 screenDir = Transform.toScreenScale(dir);

		jb.strokeWeight(strokeWeight);
		jb.stroke(r, g, b);
		jb.line(screenStart.x, screenStart.y, screenStart.add(screenDir).x, screenStart.add(screenDir).y);
	}

	/** Draw a line with coordinates 'start' and 'start'+'dir'. */
	public static void drawRay(Vector2 start, Vector2 dir) {
		drawRay(start, dir, 1);
	}

	public static void drawLine(Vector2 p1, Vector2 p2, float strokeWeight) {
		// Screen coordinates
		Vector2 s1 = Transform.toScreenPoint(p1);
		Vector2 s2 = Transform.toScreenPoint(p2);

		jb.strokeWeight(strokeWeight);
		jb.stroke(r, g, b);
		jb.line(s1.x, s1.y, s2.x, s2.y);
	}

	public static void drawLine(Vector2 p1, Vector2 p2) {
		drawLine(p1, p2, 1);
	}

	public static void drawBounds(Bounds bounds, float strokeWeight) {
		Vector2 p1 = bounds.getMin();
		Vector2 p2 = new Vector2(bounds.getMax().x, bounds.getMin().y);
		Vector2 p3 = bounds.getMax();
		Vector2 p4 = new Vector2(bounds.getMin().x, bounds.getMax().y);

		drawLine(p1, p2, strokeWeight);
		drawLine(p2, p3, strokeWeight);
		drawLine(p3, p4, strokeWeight);
		drawLine(p4, p1, strokeWeight);
	}

	public static void drawBounds(Bounds bounds) {
		drawBounds(bounds, 1);
	}

	// Draw a grid on the screen with specified columns, rows and stroke weight.
	public static void drawGrid(int columns, int rows, float strokeWeight) {
		for (int c = 0; c < columns+1; c++) {
			float x = c / (float)columns * Transform.UPW;
			drawLine(new Vector2(x, 0), new Vector2(x, Transform.UPH), strokeWeight);
		}
		for (int r = 0; r < rows + 1; r++) {
			float y = r / (float)rows * Transform.UPH;
			drawLine(new Vector2(0, y), new Vector2(Transform.UPW, y), strokeWeight);
		}
	}

	// Draw a grid on the screen with specified columns and rows.
	public static void drawGrid(int columns, int rows) {
		drawGrid(columns, rows, 1);
	}
}
