package game;

public abstract class GameObject {
	
	public static JumperBox jb = JumperBox.jb;
	
	protected String name;
	protected Transform transform;
	protected Layer layer = Layer.DEFAULT;
	
	public GameObject(String name, Vector2 position, Vector2 scale) {
		this.name = name;
		this.transform = new Transform(this, position, scale);
	}
	
	public GameObject(String name) {
		this(name, Vector2.zero(), Vector2.one());
	}
	
	public void delete() {
		Level.loadedLevel.toDelete.add(this);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Transform getTransform() {
		return transform;
	}
	
	/** Update (and draw) this object. */
	public abstract void update();

	/** Called the frame another collider collides with this objects collider (if it has one). */
	void onCollision(Collider other) {
		return;
	}
}
