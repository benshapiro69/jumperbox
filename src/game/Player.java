package game;

import processing.core.PConstants;
import processing.core.PApplet;

public class Player extends GameObject {

	static JumperBox jb = JumperBox.jb;
	
	public Vector2 velocity = new Vector2();
	public boolean facingRight = true;
	
	protected BoxCollider collider;
	protected Controller2D controller;
	
	// RGB values.
	protected int r, g, b = 0;
	protected float speed;
	protected float acceleration;
	protected float jumpHeight;
	
	protected float jumpForce;

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, String name, Vector2 position, Vector2 scale, int r, int g, int b) {
		super(name, position, scale);
		this.layer = Layer.PLAYER;
		this.collider = new BoxCollider(this);
		this.controller = new Controller2D(collider);
		this.speed = speed;
		this.acceleration = acceleration;
		this.jumpHeight = jumpHeight;
		setColor(r, g, b);
		
		this.jumpForce = PApplet.sqrt(-2 * Physics.gravity.y * jumpHeight * 1.043f); //??
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, String name, Vector2 position, Vector2 scale, int value) {
		this(speed, acceleration, jumpHeight, name, position, scale, value, value, value);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, Vector2 position, Vector2 scale, int r, int g, int b) {
		this(speed, acceleration, jumpHeight, "player", position, scale, r, g, b);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, Vector2 position, Vector2 scale, int value) {
		this(speed, acceleration, jumpHeight, position, scale, value, value, value);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, String name, int r, int g, int b) {
		this(speed, acceleration, jumpHeight, name, Vector2.zero(), Vector2.one(), r, g, b);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, String name, int value) {
		this(speed, acceleration, jumpHeight, name, value, value, value);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, int r, int g, int b) {
		this(speed, acceleration, jumpHeight, Vector2.zero(), Vector2.one(), r, g, b);
	}

	/** Construct player. */
	public Player(float speed, float acceleration, float jumpHeight, int value) {
		this(speed, acceleration, jumpHeight, value, value, value);
	}

	/** Get the RGB colors as an array. */
	public int[] getColor() {
		int[] rgb = { r, g, b };
		return rgb;
	}

	/**
	 * Set the RGB color of the object 
	 * 'r', 'g' and 'b' must be in the range [0 : 256[.
	 */
	public void setColor(int r, int g, int b) {
		if (r < 0 || r >= 256 || g < 0 || g >= 256 || b < 0 || b >= 256) {
			throw new java.lang.IllegalArgumentException("'r', 'g' and " + "'b' must be in the range [0 : 256[");
		}
		this.r = r;
		this.g = g;
		this.b = b;
	}

	/* Set the RGB color of the object all equal to value. */
	public void setColor(int value) {
		if (value < 0 || value >= 256) {
			throw new java.lang.IllegalArgumentException("'value' must " + "be in the range [0 : 256[");
		}
		r = g = b = value;
	}
	
	public BoxCollider getCollider() {
		return collider;
	}
	
	public void die() {
		Level.reload();
	}
	
	@Override
	public void onCollision(Collider other) {
		if (other.gameObject instanceof Enemy && JumperBox.playing) {
			die();
		}
	}
	
	public void jump() {
		if (controller.collisions.below) {
			velocity = velocity.add(Vector2.up().mul(jumpForce));
		}
	}
	
	public void setup() {
		controller.setup();
	}
	
	@Override
	public void delete() {
		collider.delete();
		super.delete();
	}
	
	/** Handle movement and drawing of player. */
	@Override
	public void update() {
		
		//Reset velocity if player is hitting something.
		if (controller.collisions.below) {
			velocity.y = (velocity.y < 0) ? 0 : velocity.y;
		} if (controller.collisions.above) {
			velocity.y = (velocity.y > 0) ? 0 : velocity.y;
		} if (controller.collisions.left) {
			velocity.x = (velocity.x < 0) ? 0 : velocity.x;
		} if (controller.collisions.right) {
			velocity.x = (velocity.x > 0) ? 0 : velocity.x;
		}
		
		if (jb.pressingRight && !jb.pressingLeft) {
			if (velocity.x <= speed) {
				velocity.x += acceleration / jb.frameRate;
			}
		} else if (jb.pressingLeft && !jb.pressingRight) {
			if (velocity.x >= -speed) {
				velocity.x -= acceleration / jb.frameRate;
			}
		} else {
			// If neither left nor right is pressed, de-accelerate;
			int directionX = Math.sign(velocity.x);
			float absX = Math.abs(velocity.x);
			absX = Math.max(0, absX - acceleration / jb.frameRate);
			velocity.x = absX * directionX;
		}
		// Attempt to jump if the user is telling to do so.
		if (jb.pressingJump) {
			jump();
		}
		
		velocity = velocity.add(Physics.gravity.div(jb.frameRate));
		controller.move(velocity.div(jb.frameRate));
		
		//System.out.println(transform.position.x + "\t" + transform.position.y); - debug
		show();
	}
	
	public void show() {
		// Draw the body.
		Vector2 screenPosition = transform.toScreenPoint();
		Vector2 screenScale = transform.toScreenScale();
		jb.stroke(0);
		jb.strokeWeight(1);
		jb.fill(r, g, b);
		
		jb.rectMode(PConstants.CENTER);
		jb.rect(screenPosition.x, screenPosition.y, screenScale.x, screenScale.y);
		
		// Draw the mouth and eyes.	
		Vector2 leftEyeMin = screenPosition.add(new Vector2(screenScale.mul(0.2f).x, screenScale.mul(0.11f).y));
		Vector2 leftEyeMax = screenPosition.add(new Vector2(screenScale.mul(0.45f).x, screenScale.mul(0.31f).y));
		Vector2 rightEyeMin = screenPosition.add(new Vector2(screenScale.mul(-0.05f).x, screenScale.mul(0.11f).y));
		Vector2 rightEyeMax = screenPosition.add(new Vector2(screenScale.mul(-0.3f).x, screenScale.mul(0.31f).y));
		Vector2 mouthMin = screenPosition.add(new Vector2(screenScale.mul(-0.15f).x, screenScale.mul(-0.1f).y));
		Vector2 mouthMax = screenPosition.add(new Vector2(screenScale.mul(0.3f).x, screenScale.mul(-0.3f).y));

		if (!facingRight) {
			//turns the gutterman around
			leftEyeMin.x = -(leftEyeMin.x - screenPosition.x) + screenPosition.x;
			leftEyeMax.x = -(leftEyeMax.x - screenPosition.x) + screenPosition.x;
			rightEyeMin.x = -(rightEyeMin.x - screenPosition.x) + screenPosition.x;
			rightEyeMax.x = -(rightEyeMax.x - screenPosition.x) + screenPosition.x;
			mouthMin.x = -(mouthMin.x - screenPosition.x) + screenPosition.x;
			mouthMax.x = -(mouthMax.x - screenPosition.x) + screenPosition.x;
		}
		
		
		jb.fill(0);
		jb.rectMode(PConstants.CORNERS);
		jb.rect(leftEyeMin.x, leftEyeMin.y, leftEyeMax.x, leftEyeMax.y);
		jb.rect(rightEyeMin.x, rightEyeMin.y, rightEyeMax.x, rightEyeMax.y);
		jb.rect(mouthMin.x, mouthMin.y, mouthMax.x, mouthMax.y);
	}
}
