package game;

public enum Layer {
	DEFAULT	(1),
	ENEMY	(2),
	PLAYER	(4);
	
	private final int layerNum;

    private Layer(int layerNum) {
        this.layerNum = layerNum;
    }
    
    public int getLayerNum() {
    	return this.layerNum;
    }
    
    /** Get a number that encompass all layers. */
    public static int all() {
    	Layer[] values = Layer.values();
    	int sum = 0;
    	
    	for (int i = 0; i < values.length; i++) {
    		sum += values[i].getLayerNum();
    	}
    	
    	return sum;
    }
    
    /** Does 'mask' have a bit in common with this layer? */
    public boolean encompass(int mask) {
    	return (this.layerNum & mask) != 0;
    }
}
