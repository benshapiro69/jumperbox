package game;

/** Collision and movement handling.
 * 
 *  Credit to Sebastian Lague for showing how to implement this in Unity.
 *  https://www.youtube.com/channel/UCmtyQOKKmrMVaKuRXz02jbQ*/
public class Controller2D {
	
	static final float skinWidth = .015f;
	
	int horizontalRayCount;
	int verticalRayCount;
	float horizontalRaySpacing;
	float verticalRaySpacing;

	BoxCollider collider;
	RaycastOrigins raycastOrigins;
	Bounds bounds;
	
	public CollisionInfo collisions = new CollisionInfo();
	
	public Controller2D (BoxCollider collider, 
						 int horizontalRayCount, int verticalRayCount) {
		this.raycastOrigins = new RaycastOrigins();
		this.collider = collider;
		this.horizontalRayCount = horizontalRayCount < 2 ? 2 : horizontalRayCount;
		this.verticalRayCount = verticalRayCount < 2 ? 2 : verticalRayCount;
		
		updateRaycastOrigins();
		calulateRaySpacing();
	}
	
	public Controller2D (BoxCollider collider) {
		this(collider, 4, 4);
	}
	
	public void setup() {
		calulateRaySpacing();
	}
	
	public Vector2 doCollisions (Vector2 velocity) {
		updateRaycastOrigins();
		collisions.reset();
		
		if (velocity.x != 0) {
			velocity = horizontalCollision(velocity);
		}
		if (velocity.y != 0) {
			velocity = verticalCollision(velocity);
		}
		
		return velocity;
	}
	
	public void move(Vector2 velocity) {
		velocity = doCollisions(velocity);
		collider.gameObject.transform.translate(velocity);
	}
	
	Vector2 horizontalCollision(Vector2 velocity) {
		int layerMask = collider.gameObject.layer.getLayerNum();
		// The signed value of the horizontal velocity.
		float directionX = Math.sign(velocity.x);
		// The absolute value of the horizontal velocity.
		float rayLength = Math.abs(velocity.x);
		rayLength += skinWidth;
		
		for (int i = 0; i < horizontalRayCount; i++) {
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;

			rayOrigin = rayOrigin.add(Vector2.up().mul(horizontalRaySpacing * i));
			HitInfo[] hits = Physics.raycast(rayOrigin, Vector2.right().mul(directionX), rayLength, layerMask);
			
			if (hits.length != 0) {
				for (HitInfo hit : hits) {
					float newVelX = (hit.distance - skinWidth) * directionX;
					if (Math.abs(velocity.x) >= Math.abs(newVelX)) {
						
						// Call the onCollision
						collider.gameObject.onCollision(hit.collider);
						hit.collider.gameObject.onCollision(this.collider);
						
						if (!hit.collider.isTrigger) {
							velocity.x = newVelX;
							// Done to prevent future rays hitting something farther away.
							rayLength = hit.distance;
							
							collisions.left = directionX == -1;
							collisions.right = directionX == 1;
						}
					}
				}
			}
		}
		
		return velocity;
	}
	
	Vector2 verticalCollision(Vector2 velocity) {
		int layerMask = collider.gameObject.layer.getLayerNum();
		// The signed value of the vertical velocity.
		float directionY = Math.sign(velocity.y);
		// The absolute value of the vertical velocity.
		float rayLength = Math.abs(velocity.y);
		rayLength += skinWidth;
		
		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
			// Add velocity.x to cast ray from where the object will be after movement.
			rayOrigin = rayOrigin.add(Vector2.right().mul(verticalRaySpacing * i + velocity.x));
			HitInfo[] hits = Physics.raycast(rayOrigin, Vector2.up().mul(directionY), rayLength, layerMask);
			
			if (hits.length != 0) {
				for (HitInfo hit : hits) {
					float newVelY = (hit.distance - skinWidth) * directionY;
					if (Math.abs(velocity.y) >= Math.abs(newVelY)) {
						collider.gameObject.onCollision(hit.collider);
						if (!hit.collider.isTrigger) {
							velocity.y = newVelY;
							// Done to prevent future rays hitting something farther away.
							rayLength = hit.distance;
							
							collisions.below = directionY == -1;
							collisions.above = directionY == 1;
						}
					}
				}
			}
		}
		
		return velocity;
	}
	
	void updateRaycastOrigins () {
		bounds = collider.bounds();
		bounds.expand(skinWidth * -2);
		
		raycastOrigins.bottomLeft = new Vector2(bounds.getMin().x,bounds.getMin().y);
		raycastOrigins.bottomRight = new Vector2(bounds.getMax().x,bounds.getMin().y);
		raycastOrigins.topLeft = new Vector2(bounds.getMin().x,bounds.getMax().y);
		raycastOrigins.topRight = new Vector2(bounds.getMax().x,bounds.getMax().y);
		
	}
	
	void calulateRaySpacing() {
		bounds = new Bounds(collider.bounds().getCentre(), collider.bounds().getSize());
		bounds.expand(skinWidth * -2);
		
		horizontalRaySpacing = bounds.getSize().y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.getSize().x / (verticalRayCount - 1);
	}
	
	public int getHorizontalRayCount() {
		return horizontalRayCount;
	}
	
	public int getVerticalRayCount() {
		return verticalRayCount;
	}
	
	class RaycastOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}
	
	/** Info about where collision are occurring. */
	public class CollisionInfo {
		public boolean below, above;
		public boolean left, right;
		
		public void reset() {
			below = above = false;
			left = right = false;
		}
	}
}
