package game;

import processing.core.PApplet;
import processing.core.PImage;

/** Class that deletes game objects when the player comes into contact. */
public class Lever extends GameObject {

	static String imagePath = "data/lever.png";
	static String imageFlippedPath = "data/lever-flipped.png";

	BoxCollider collider;
	boolean activated = false;
	GameObject[] deleteObjects;
	
	PImage leverImage;
	PImage leverFlippedImage;

	public Lever(String name, Vector2 position, Vector2 scale, GameObject[] deleteObjects) {
		super(name, position, scale);
		this.deleteObjects = deleteObjects;
		collider = new BoxCollider(this, true);

		leverImage = jb.loadImage(imagePath);
		leverFlippedImage = jb.loadImage(imageFlippedPath);
		
		leverImage.resize((int)Transform.toScreenScale(scale).x,
				 -(int)Transform.toScreenScale(scale).y);
		leverFlippedImage.resize((int)Transform.toScreenScale(scale).x,
				 -(int)Transform.toScreenScale(scale).y);
	}

	public Lever(Vector2 position, Vector2 scale, GameObject[] deleteObjects) {
		this("lever", position, scale, deleteObjects);
	}

	@Override
	public void delete() {
		collider.delete();
		super.delete();
	}

	void activate() {
		// Do not activate if not activated.
		if (activated) {
			return;
		}
		activated = true;
		for (int i = 0; i < deleteObjects.length; i++) {
			deleteObjects[i].delete();
			deleteObjects[i] = null;
		}
	}

	@Override
	public void update() {
		if (Bounds.overlaps(collider.bounds(), Level.loadedLevel.player.getCollider().bounds())) {
			activate();
		}
		show();
	}

	public void show() {
		jb.imageMode(PApplet.CENTER);
		float imgx = transform.toScreenPoint().x;
		float imgy = transform.toScreenPoint().y;
		
		if (activated) {
			jb.image(leverFlippedImage, imgx, imgy);
		} else {
			jb.image(leverImage, imgx, imgy);
		}
	}
}
