package game;

import java.util.ArrayList;

/** Handling physics and raycasting. */
public class Physics {
	
	static Vector2 gravity = Vector2.down().mul(20);
	
	/** Intersect a ray against all colliders not encompassed in layerMask
	 * and cull all intersection points farther than maxDistance
	 * with respects to the ray origin. Return a HitInfo instance for each
	 * intersection point.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. */
	public static HitInfo[] raycast(Ray ray, float maxDistance, int layerMask) {
		
		ArrayList<HitInfo> hits = new ArrayList<HitInfo>();
		
		// Iterate through all colliders in the scene.
		for (Collider collider : Collider.colliders) {
			// If 'layerMask' encompasses the layer of the game object
			// of the collider, it should be skipped.
			if (collider.gameObject.layer.encompass(layerMask)) {
				continue;
			}
			// Get the intersection points.
			Vector2[] colliderIntersections = collider.rayIntersection(ray);
			// Loop through intersections, and add the ones that are not too far away.
			for (Vector2 I : colliderIntersections) {
				float distance = ray.getOrigin().distance(I);
				if (distance <= maxDistance) {
					HitInfo hit = new HitInfo(collider, distance, I);
					hits.add(hit);
				}
			}
		}
		
		// Sort the vectors.
		hits.sort(new HitDistanceComparator());
		
		HitInfo[] hitArray = new HitInfo[hits.size()];
		return hits.toArray(hitArray);
	}
	
	/** Intersect a ray against all colliders a distance of maxDistance or less.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Ray ray, float maxDistance) {
		// Pass layerMask=0 which ignores no layers.
		return raycast(ray, maxDistance, 0);
	}
	
	/** Intersect a ray against all colliders not encompassed in layerMask.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Ray ray, int layerMask) {
		return raycast(ray, Float.POSITIVE_INFINITY, layerMask);
	}
	
	/** Intersect a ray against all colliders.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. */
	public static HitInfo[] raycast(Ray ray) {
		return raycast(ray, Float.POSITIVE_INFINITY, 0);
	}
	
	/** Intersect a ray defined by 'origin' and 'direction' against 
	 * all colliders not encompassed in layerMask
	 * and cull all intersection points farther than maxDistance
	 * with respects to the ray origin.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Vector2 origin,Vector2 direction, 
									float maxDistance, 
									int layerMask) {
		Ray ray = new Ray(origin, direction);
		return raycast(ray, maxDistance, layerMask);
	}
	
	/** Intersect a ray defined by 'origin' and 'direction' against all 
	 * colliders and cull all intersection points farther than maxDistance
	 * with respects to the ray origin.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Vector2 origin, Vector2 direction, float maxDistance) {
		Ray ray = new Ray(origin, direction);
		// Pass layerMask=0 which ignores no layers.
		return raycast(ray, maxDistance, 0);
	}
	
	/** Intersect a ray defined by 'origin' and 'direction' against all 
	 * colliders not encompassed in layerMask.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Vector2 origin, Vector2 direction, int layerMask) {
		Ray ray = new Ray(origin, direction);
		return raycast(ray, Float.POSITIVE_INFINITY, layerMask);
	}
	
	/** Intersect a ray defined by 'origin' and 'direction' against all 
	 * colliders.
	 * 
	 * The intersection points are sorted from closest to farthest from the
	 * ray origin. Return a HitInfo instance for each
	 * intersection point.*/
	public static HitInfo[] raycast(Vector2 origin, Vector2 direction) {
		Ray ray = new Ray(origin, direction);
		return raycast(ray, Float.POSITIVE_INFINITY, 0);
	}
}
