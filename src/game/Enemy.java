package game;

public abstract class Enemy extends GameObject {
	
	Collider collider;
	
	public Enemy(String name, Vector2 position, Vector2 scale) {
		super(name, position, scale);
	}
	
	@Override
	public void delete() {
		super.delete();
		collider.delete();
	}
	
	// Dust the player if they come into contact.
	@Override
	void onCollision(Collider other) {
		if (other.gameObject instanceof Player) {
			Player player = (Player)other.gameObject;
			player.die();
		}
	}
}
